export interface BlogPostRaw {
    userId:number;
    id:number;
    title:string;
    body:string;
}
