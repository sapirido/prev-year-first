export interface SavedPost {
    uid:string;
    postId:number;
    title:string;
    body:string;
    likes?:number;
}
