import { AuthService } from './../auth.service';
import { Observable } from 'rxjs';
import { BlogPostRaw } from './../interfaces/blog-post-raw';
import { Component, OnInit } from '@angular/core';
import { SavedPost } from '../interfaces/saved-post';
import { BlogService } from '../blog.service';
import {DomSanitizer} from '@angular/platform-browser';
import {MatIconRegistry} from '@angular/material/icon';



const THUMBUP_ICON = `
  <svg xmlns="http://www.w3.org/2000/svg" width="24px" height="24px">
    <path d="M0 0h24v24H0z" fill="none"/>
    <path d="M1 21h4V9H1v12zm22-11c0-1.1-.9-2-2-2h-6.31l.95-4.57.03-.32c0-.41-.17-.79-.` +
      `44-1.06L14.17 1 7.59 7.59C7.22 7.95 7 8.45 7 9v10c0 1.1.9 2 2 2h9c.83 0 1.54-.5` +
      `1.84-1.22l3.02-7.05c.09-.23.14-.47.14-.73v-1.91l-.01-.01L23 10z"/>
  </svg>
`;


@Component({
  selector: 'app-saved-posts',
  templateUrl: './saved-posts.component.html',
  styleUrls: ['./saved-posts.component.css']
})

export class SavedPostsComponent implements OnInit {
  savedPosts:SavedPost[] = [];
  savedPosts$:Observable<any[]>;
 displayedColumns: string[] = ['id','title','body','likes','actions'];
 userId:string;

  constructor(private blogService:BlogService,private auth:AuthService,iconRegistry: MatIconRegistry, sanitizer: DomSanitizer) {
    iconRegistry.addSvgIconLiteral('thumbs-up', sanitizer.bypassSecurityTrustHtml(THUMBUP_ICON)); 
   }

   deletePost(postId){
    this.blogService.deletePost(this.userId,postId);
   }

   updateLikes(postId,currentLikes){
     const likes = currentLikes ? currentLikes : 0;
     this.blogService.updateLikes(this.userId,postId,likes);
   }

  ngOnInit():void {
    this.auth.getUser().subscribe(
      user => {
        this.userId = user.uid;
        this.savedPosts$ = this.blogService.getSavedPosts(user.uid);
        this.savedPosts$.subscribe(
          docs => {
            this.savedPosts = [];
            for(let document of docs){
                const savedPost:SavedPost = document.payload.doc.data();
                savedPost.postId = document.payload.doc.id;
                this.savedPosts.push(savedPost);
            }
          }
        )
      }
    )
  }

}
