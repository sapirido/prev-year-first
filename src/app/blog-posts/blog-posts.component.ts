import { AuthService } from './../auth.service';
import { BlogPost } from './../interfaces/blog-post';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { Component, OnInit } from '@angular/core';
import { BlogService } from '../blog.service';

@Component({
  selector: 'app-blog-posts',
  templateUrl: './blog-posts.component.html',
  styleUrls: ['./blog-posts.component.css']
})
export class BlogPostsComponent implements OnInit {
  blogPostData$:Observable<BlogPost[]>;
  blogPosts:BlogPost[] = [];
  panelOpenState:boolean = false;
  userId:string;

  constructor(private blogPostService:BlogService,
              private router:Router,private auth:AuthService) { }

  postComments(postId){
    this.router.navigate(['/postComments',postId]);
  }

  savePost(post:BlogPost){
    this.blogPostService.savePost(this.userId,post);
  }

  ngOnInit(): void {
    this.auth.getUser().subscribe(
      user =>this.userId = user.uid
    )
    this.blogPostData$ = this.blogPostService.getBlogPosts();
    this.blogPostData$.subscribe(
      posts =>{
        console.log({posts});
        this.blogPosts = posts;
      } 
    )
    
  }

}
