import { BlogPostRaw } from './interfaces/blog-post-raw';
import { BlogPost } from './interfaces/blog-post';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError, map } from 'rxjs/operators';
import { Observable,throwError } from 'rxjs';
import { CommentRaw } from './interfaces/comment-raw';
import { Comment } from './interfaces/comment'
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
export class BlogService {

 userCollection:AngularFirestoreCollection = this.db.collection('users');
 postsCollection:AngularFirestoreCollection;

  constructor(private db:AngularFirestore,private http:HttpClient) { }
//POSTS
  getBlogPosts():Observable<BlogPost[]>{
    return this.http.get<BlogPostRaw[]>("https://jsonplaceholder.typicode.com/posts").pipe(
      map(data => this.transformPostData(data)),
      catchError(this.handleError)
    )
  }
  
  transformPostData(data:BlogPostRaw[]):BlogPost[]{  
  
  const blogPostArray:BlogPost[]  = data.map(raw => {
    return {
      id:raw.id,
      title:raw.title,
      body:raw.body
    }
  }
    )
    return blogPostArray;
  }

  savePost(userId:string,post:BlogPost){
   this.userCollection.doc(userId).collection('posts').add(post);
  return  this.userCollection.snapshotChanges();
  }

  deletePost(userId,postId){
    this.postsCollection =  this.userCollection.doc(userId).collection('posts')
    this.postsCollection.doc(postId).delete();
    return this.postsCollection.snapshotChanges();
  }

  updateLikes(userId,postId,likes){
    this.postsCollection = this.userCollection.doc(userId).collection('posts');
    this.postsCollection.doc(postId).update({
      likes:likes + 1
    })
   return this.postsCollection.snapshotChanges();
  }

  getSavedPosts(userId:string){
    this.postsCollection = this.userCollection.doc(userId).collection('posts');
    return this.postsCollection.snapshotChanges();
  }

  //COMMENT

  getCommentByPostId(postId:number):Observable<Comment[]>{
    return this.http.get<CommentRaw[]>(`https://jsonplaceholder.typicode.com/comments?postId=${postId}`).pipe(
      map(data => this.transformCommentData(data),
      catchError(this.handleError)
      )
    )
  }
  
  transformCommentData(data:CommentRaw[]):Comment[]{
   const comments:Comment[] =  data.map(raw => {
   return{
      id:raw.id,
      name:raw.name,
      email:raw.email,
      body:raw.body,
    }
  })

  return comments;
  }

  public handleError(res:HttpErrorResponse){
   console.log(res.error);
   return throwError(res.error || 'Server error');
  }
}
