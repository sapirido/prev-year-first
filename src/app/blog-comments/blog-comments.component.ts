import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { BlogPost } from './../interfaces/blog-post';
import { Component, OnInit } from '@angular/core';
import { BlogService } from '../blog.service';
import {Comment} from '../interfaces/comment';

@Component({
  selector: 'app-blog-comments',
  templateUrl: './blog-comments.component.html',
  styleUrls: ['./blog-comments.component.css']
})

export class BlogCommentsComponent implements OnInit {
  comments:Comment[] = [];
  comments$:Observable<Comment[]>;
  
  constructor(private blogService:BlogService,private route:ActivatedRoute) { }

  ngOnInit(): void {
    const postId = this.route.snapshot.params.postId;
    this.comments$ = this.blogService.getCommentByPostId(postId);
    this.comments$.subscribe(
      comments => this.comments = comments
    )
  }


}
