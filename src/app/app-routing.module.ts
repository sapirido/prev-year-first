
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { LoginSuccessfulComponent } from './login-successful/login-successful.component';
import { BlogPostsComponent } from './blog-posts/blog-posts.component';
import { BlogCommentsComponent } from './blog-comments/blog-comments.component';
import { SavedPostsComponent } from './saved-posts/saved-posts.component';

const routes: Routes = [
  {path: 'login', component:LoginComponent},
  { path: 'register', component: RegisterComponent },
  {path: 'login/success', component: LoginSuccessfulComponent},
  {path: 'blogPosts', component: BlogPostsComponent},
  {path: 'postComments/:postId', component: BlogCommentsComponent},
  {path: 'saved-posts', component: SavedPostsComponent},
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
