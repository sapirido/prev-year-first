// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyCUUBvpclUiB-QpBFYdF3q8U8KXZ3zm-4E",
    authDomain: "prev-year-first.firebaseapp.com",
    projectId: "prev-year-first",
    storageBucket: "prev-year-first.appspot.com",
    messagingSenderId: "631696721657",
    appId: "1:631696721657:web:4d8bb7cea1cf010c418428"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
